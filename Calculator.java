/**
 * @author Samuel Mathieu
 * Class ID: 479
 * Class Description:
 * This class takes an integer total, and applies operations
 * to the total with each function
 * @version 3.0
 */
package cse360assign2;

public class Calculator {

	private int total;
	private StringBuffer history;
	
	/**
	 * This constructor initializes the total to zero
	 */
	public Calculator () {
		total = 0;  // not needed - included for clarity
		history = new StringBuffer("0");
	}
	
	/**
	 * This function returns the total 
	 * @return the value of total variable
	 */
	public int getTotal () {
		return total;
	}
	
	/**
	 * This function adds the value parameter to the total
	 * and adds "+" the value to the string buffer for the get history function
	 * @param value to be added to total
	 */
	public void add (int value) {
		total = value + total;
		history.append(" + " + value);
	}
	
	/**
	 * This function subtracts the value parameter to the total
	 * and adds "-" the value to the string buffer for the get history function
	 * @param value to be subtracted to total
	 */
	public void subtract (int value) {
		total = total - value;
		history.append(" - " + value);

	}
	
	/**
	 * This function multiplies the value parameter to the total
	 * and adds "*" the value to the string buffer for the get history function
	 * @param value to be multiplied to total
	 */
	public void multiply (int value) {
		total = value * total;
		history.append(" * " + value);

	}
	
	/**
	 * This function divides the value parameter to the total
	 * and adds "/" the value to the string buffer for the get history function
	 * @param value to be divided from value
	 */
	public void divide (int value) {
		if(value == 0)
		{
			total = 0;
		}
		else
		{
			total = total / value;
			history.append(" / " + value);

		}
	}
	
	/**
	 * This function returns the history of operations performed on total variable
	 * from the values appended to the string buffer and returns it as a string using the toString function
	 * @return a string of the operations performed to the total variable
	 */
	public String getHistory () {
		return history.toString();
	}
}
